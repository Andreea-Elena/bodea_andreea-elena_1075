
const FIRST_NAME = "Andreea-Elena";
const LAST_NAME = "Bodea";
const GRUPA = "1075";

/**
 * Make the implementation here
 */
function initCaching() {
    var object={}; 
    object.home=0;
    object.about=0;
    object.contact=0;    

    object.pageAccessCounter=function(value){
        if((value=='home')||(value=='HOME'))
        object.home++;

        if((value=='about')||(value=='ABOUT'))
        object.about++;

        if((value=='contact')||(value=='CONTACT'))
        object.contact++;

        if(value===undefined)
        object.home++;

    }
    
    object.getCache=function(){
        return this
    }
    

    return object;
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

